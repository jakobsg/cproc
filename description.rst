***********************
Capture Process (cproc)
***********************

Capture the environment and other key values about a running process into an cproc
info file (JSON).
It is meant as a tool for system administration to do system probing.
A captured process info file can be used as input to another program or do other
things like respawning the process in the same type of environment.

